#!/usr/bin/env python3
from devilparser import rcfile
import sys
from dukeservicenowpy.servicenow import api


def main():
    if len(sys.argv) != 2:
        sys.stderr.write("Usage: %s netid\n" % sys.argv[0])
        sys.exit(1)
    sn_instance = 'duke'
    configinfo = rcfile.parse('~/.servicenow.yaml', sn_instance).contents()
    cred = {}
    cred['username'] = configinfo['username']
    cred['password'] = configinfo['password']
    SN = api('%s.service-now.com' % 'duke', cred)
    user = SN.get_user_from_name(sys.argv[1])

    for group in user.get_groups():
        print(group.json())


if __name__ == "__main__":
    sys.exit(main())
