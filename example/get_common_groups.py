#!/usr/bin/env python3
from devilparser import rcfile
import sys
from dukeservicenowpy.servicenow import api


def main():
    if len(sys.argv)  < 3:
        sys.stderr.write("Usage: %s netid netid [netid...]\n" % sys.argv[0])
        sys.exit(1)
    sn_instance = 'duke'
    configinfo = rcfile.parse('~/.servicenow.yaml', sn_instance).contents()
    cred = {}
    cred['username'] = configinfo['username']
    cred['password'] = configinfo['password']
    SN = api('%s.service-now.com' % 'duke', cred)
    netids = sys.argv[1:]

    print(SN.find_common_groups(*netids))


if __name__ == "__main__":
    sys.exit(main())
