#!/usr/bin/env python3
from devilparser import rcfile
import sys
from dukeservicenowpy.servicenow import api


def main():
    if len(sys.argv) != 2:
        sys.stderr.write("Usage: %s netid\n" % sys.argv[0])
        sys.exit(1)
    sn_instance = 'dukesandbox'
    configinfo = rcfile.parse('~/.servicenow.yaml', sn_instance).contents()
    cred = {}
    cred['username'] = configinfo['username']
    cred['password'] = configinfo['password']
    SN = api('%s.service-now.com' % sn_instance, cred)
    group = SN.get_group(by='name', value=sys.argv[1])

    print(group)
    print(len(group.get_users()))


if __name__ == "__main__":
    sys.exit(main())
