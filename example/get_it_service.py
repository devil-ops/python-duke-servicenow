#!/usr/bin/env python3
from devilparser import rcfile
import sys
from dukeservicenowpy.servicenow import api


def main():
    if len(sys.argv) != 3:
        sys.stderr.write("Usage: %s service_id_name organization_name\n" %
                         sys.argv[0])
        sys.exit(1)
    sn_instance = 'dukesandbox'
    configinfo = rcfile.parse('~/.servicenow.yaml', sn_instance).contents()
    cred = {}
    cred['username'] = configinfo['username']
    cred['password'] = configinfo['password']
    SN = api('%s.service-now.com' % sn_instance, cred)

    it_service_name = sys.argv[1]
    organization_name = sys.argv[2]

    org = SN.get_organization(by='u_name', value=organization_name)

    item = SN.get_it_service(by='name', value=it_service_name,
                             organization=org)

    print(item)


if __name__ == "__main__":
    sys.exit(main())
