#!/usr/bin/env python3
from devilparser import rcfile
import sys
from dukeservicenowpy.servicenow import api
import argparse


def parse_args():
    parser = argparse.ArgumentParser(description='Create a SN Incident')
    parser.add_argument('-g', '--group', help='Assing to a specific SN group')

    return parser.parse_args()


def main():
    args = parse_args()
    sn_instance = 'dukesandbox'
    configinfo = rcfile.parse('~/.servicenow.yaml', sn_instance).contents()
    cred = {}
    cred['username'] = configinfo['username']
    cred['password'] = configinfo['password']
    SN = api('%s.service-now.com' % (sn_instance), cred)

    ticket = SN.create_task_ticket(
        description='Here are the details of the ticket',
        short_description='This is a ticket',
        organization='Duke University',
        assignment_group=args.group
    )

    print(ticket)
    print(ticket.json())


if __name__ == "__main__":
    sys.exit(main())
