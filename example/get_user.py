#!/usr/bin/env python3
from devilparser import rcfile
import sys
from dukeservicenowpy.servicenow import api


def main():
    if len(sys.argv) != 2:
        sys.stderr.write("Usage: %s netid\n" % sys.argv[0])
        sys.exit(1)
    sn_instance = 'dukesandbox'
    configinfo = rcfile.parse('~/.servicenow.yaml', sn_instance).contents()
    cred = {}
    cred['username'] = configinfo['username']
    cred['password'] = configinfo['password']
    SN = api('%s.service-now.com' % sn_instance, cred)
    user = SN.get_user(by='name', value=sys.argv[1])
    print(user)
    print(user.json())
    for group in user.get_groups(attribute='name'):
        print(group)


if __name__ == "__main__":
    sys.exit(main())
