#!/usr/bin/env python3
from devilparser import rcfile
import sys
from dukeservicenowpy.servicenow import api


def main():
    if len(sys.argv) != 2:
        sys.stderr.write("Usage: %s netid\n" % sys.argv[0])
        sys.exit(1)
    sn_instance = 'duke'
    configinfo = rcfile.parse('~/.servicenow.yaml', sn_instance).contents()
    cred = {}
    cred['username'] = configinfo['username']
    cred['password'] = configinfo['password']
    SN = api('%s.service-now.com' % 'duke', cred)
    user = SN.get_user_from_name(sys.argv[1])

    tasks = user.get_tasks(active=False)
    print("Users active tasks")
    print(tasks)

    print("User task statistics")
    print(user.get_task_statistics())

    print("Most active group is")
    print(user.get_most_active_group())


if __name__ == "__main__":
    sys.exit(main())
