"""
Helper functions
"""
import os
import sys
import logging
class CredentialParser(object):
    """
    Import credentials for connecting to Clockworks

    This file should look something like:
    [https://clockworks-test.oit.duke.edu]
    username = 'username'
    apikey = 'XXXX'
    apiprefix = '/api/v1/vm_api'
    """

    def __init__(self, base_url):
        self.base_url = base_url

    def import_credentials(self):
        """
    	Pull in your credentials
    	"""
        cred_file = os.path.expanduser("~/.servicenowrc")
        if not os.path.exists(cred_file):
            sys.exit("%s is required" % cred_file)
        import configparser
        config = configparser.ConfigParser()
        config.read(cred_file)

        creds = {}
        creds['username'] = config.get(self.base_url, "username")
        creds['password'] = config.get(self.base_url, "password")
        return creds

def pp_results(results):
    """
    Print list of results (or just results)
    """
    if isinstance(results, list):
        for result in results:
            pp_result(result)
    else:
        pp_result(results)

def pp_result(result):
    """
    Print out json results in a pretty way (Pretty Print)
    """
    from datetime import timedelta
    import humanize
    for name, value in result.items():

        ## Skip empty junk
        if not value:
            continue

        if name == 'ttl':
            natty_time = humanize.\
                naturaltime(timedelta(seconds=int(value))).\
                replace(" ago", "")
            hr_value = '%s (or %s seconds)' % (natty_time, value)
        elif name == 'last_registered':
            hr_value = naturalize_day(value)
        elif name == 'last_unregistered':
            hr_value = naturalize_day(value)
        elif name == 'last_seen':
            hr_value = naturalize_day(value)
        else:
            hr_value = value
        print("%-20s %-5s" % (name, hr_value))

def naturalize_day(value):
    """
    Given an ISO date, return something human readable
    """
    import humanize
    from dateutil import parser
    try:
        natty_time = humanize.naturalday(parser.parse(value))
    except:
        natty_time = value
    hr_value = '%s (%s)' % (natty_time, value)
    return hr_value
