import logging
import sys
from dukeservicenowpy.models import User, Group, Incident, Organization
from dukeservicenowpy.models import RequestedItem
from dukeservicenowpy.models import ItService
import requests
import requests_cache
from datetime import timedelta


class api(object):
    """
    API Object
    """

    @classmethod
    def __init__(self, base_url, credentials, *args, **kwargs):
        self.base_url = base_url
        self.credentials = (credentials['username'], credentials['password'])
        cache_lifetime = kwargs.get('cache_lifetime', timedelta(hours=1))

        requests_cache.install_cache('sn_cache', expire_after=cache_lifetime)

    def get_ritms_from_req(self, req_sys_id):
        return self.query_table(
            'sc_req_item', params={
                'sysparm_query': 'request=%s' % req_sys_id
            }
        )

    def get_tasks_from_ritm(self, ritm_sys_id):
        return self.query_table(
            'sc_task', params={
                'sysparm_query': 'request_item=%s' % ritm_sys_id
            }
        )

    def get_tasks_from_req(self, req_sys_id, only_open=True):
        tasks = []
        for ritm in self.get_ritms_from_req(req_sys_id):
            for task in self.get_tasks_from_ritm(ritm['sys_id']):
                if only_open:
                    if self.get_state_name('task', task['state']) == 'Open':
                        tasks.append(task)
        return tasks

    def get_state_name(self, table_type, state_id):
        return self.query_table(
            'sys_choice', params={
                'name': table_type,
                'element': 'state',
                'value': state_id
            }
        )

    def get_state_id(self, table_type, state_label):
        possible_ids = range(0, 10)
        for possible_id in possible_ids:
            try:
                possible_name = self.get_state_name(table_type, possible_id)
            except Exception as e:
                sys.stderr.write("Caught exception %s\n" % e)
                continue
            if possible_name == state_label:
                return possible_id

        raise Exception("InvalidStateLabel")

    def get_user_from_name(self, name):
        """
        :param name: netid of object to get
        :type name: str
        :returns: User object for name
        :rtype: User
        """
        results = self.query_table('sys_user?user_name=%s' % name)
        if len(results) != 1:
            raise("Did not get just one result from the query_table")
        return User(api=self, **results[0])

    def find_common_groups(self, *args, **kwargs):
        group_sets = []
        for netid in args:

            # If this is just a string, assume only the netid was passed in,
            # not a user object

            if isinstance(netid, str):
                user = self.get_user_from_name(netid)
            else:
                user = netid
            group_sets.append(user.get_groups(attribute='name'))
        intersect = set(group_sets[0]).intersection(*group_sets)
        return list(intersect)

    def get_organization(self, *args, **kwargs):
        """
        :param by: Key to lookup organization on
        :type by: str
        :param value: Value of the key to search on
        :type value: str
        :return: Matching organization object
        :rtype: Organization
        """
        by = kwargs.get('by')
        value = kwargs.get('value')

        results = self.query_table('u_organization?%s=%s' % (by, value))
        if len(results) != 1:
            sys.stderr.write("Got %s results, not 1\n" % len(results))
            raise Exception("WrongNumOfResults")
        return Organization(api=self, **results[0])

    def get_it_service(self, *args, **kwargs):
        """
        :param by: Key to lookup organization on
        :type by: str
        :param value: Value of the key to search on
        :type value: str
        :return: Matching it_service object
        :param organization: Organization for it service
        :type organization: Organization
        :rtype: ItService
        """
        by = kwargs.get('by')
        value = kwargs.get('value')
        organization = kwargs.get('organization')

        results = self.query_table(
            'cmdb_ci_service?%s=%s&u_organization_type=%s' % (
                by, value, organization.sys_id))
        if len(results) != 1:
            sys.stderr.write("Got %s results, not 1\n" % len(results))
            raise Exception("WrongNumOfResults")
        return ItService(api=self, **results[0])

    def get_user(self, *args, **kwargs):
        """
        :param by: Key to lookup user on
        :type by: str
        :param value: Value of the key to search on
        :type value: str
        :return: Matching user object
        :rtype: User
        """
        by = kwargs.get('by')
        value = kwargs.get('value')

        results = self.query_table('sys_user?%s=%s' % (by, value))
        if len(results) != 1:
            sys.stderr.write("Got %s results, not 1\n" % len(results))
            raise Exception("WrongNumOfResults")
        return User(api=self, **results[0])

    def get_group(self, *args, **kwargs):
        """
        :param by: Key to lookup group on
        :type by: str
        :param value: Value of the key to search on
        :type value: str
        :return: Matching group object
        :rtype: Group
        """
        by = kwargs.get('by')
        value = kwargs.get('value')
        params = {
            by: value
        }

        results = self.query_table('sys_user_group', params=params)
        if len(results) != 1:
            raise Exception("WrongNumOfResults")
        return Group(api=self, **results[0])

    def get_group_from_sys_id(self, sys_id):
        """
        Using the groups sys_id, return a group object

        :param sys_id: The sys_id for the group
        :type sys_id: str
        :return: Object for the group
        :rtype: Group
        """
        results = self.query_table('sys_user_group?sys_id=%s' % sys_id)
        if len(results) != 1:
            raise("Did not get just one result from the query_table")
        return Group(api=self, **results[0])

    def build_sysparm_query(self, params):
        """
        Given a list of filters, generate a sysparm query
        """
        return "^".join(params)

    def get_tasks_for_user(self, username):
        user_sys_id = self.get_user_from_name(username)['sys_id']
        sysparm_query = self.build_sysparm_query([
            'assigned_to=%s' % user_sys_id,
            'active=true',
            'ref_incident.incident_stateNOT IN6,7',
            'ORref_incident.incident_stateISEMPTY',
            'sys_class_name!=sc_request',
            'sys_class_name!=sc_req_item',
            'sys_class_name!=u_survey_response_reporting',
            'sys_class_name!=chat_queue_entry'
        ])
        return self.query_table(
            'task',
            params={'sysparm_query': sysparm_query}
        )

    def create_task_ticket(self, *args, **kwargs):
        """
        Create a task in ServiceNow

        Args:
        short_description str: Short description for the ticket
        description       str: Long description for the ticket
        state_id          int: State of ticket  (default: 2 (active))
        impact_id         int: Impact of ticket (default: 2 (workgroup))
        urgency_id        int: Urgency of ticket (default: 2 (medium))
        priority_id       int: Priority of ticket (default: 3 (medium))
        assignment_group  str: Assignment group for ticket (default: None)
        caller_name       str: Name of caller (default: 'Joe User')
        service_offering  str: Service Offering
                               (default: Virtual Server Hosting Offering')
        it_service        str: It Service (default: Virtual Server Hosting)
        organization      str: Organization name (default: Duke University)


        Returns:
        Incident object

        """

        short_description = kwargs.get('short_description')
        description = kwargs.get('description')
        state_id = kwargs.get('state_id', 2)
        impact_id = kwargs.get('impact_id', 2)
        urgency_id = kwargs.get('urgency_id', 2)
        priority_id = kwargs.get('priority_id', 3)
        assignment_group = kwargs.get('assignment_group', None)
        caller_name = kwargs.get('caller_name', 'Joe User')
        it_service = kwargs.get('it_service', 'Virtual Server Hosting')
        service_offering = kwargs.get(
            'service_offering', 'Virtual Server Hosting Offering')
        organization_name = kwargs.get('organization', 'Duke University')

        if assignment_group:
            assignment_group_id = self.get_group(
                by='name', value=assignment_group).sys_id

        organization = self.get_organization(by='u_name',
                                             value=organization_name)

        # TODO: Convert services to objects and use that
        it_service_obj = self.get_it_service(by='name', value=it_service,
                                             organization=organization)
        service_offering_id = self.query_table('service_offering?name=%s' %
                                               service_offering)[0]['sys_id']

        ticket_data = {
#           'urgency': urgency_id,
#           'contact_type': 'event',
#           'incident_state': state_id,
            'short_description': short_description,
            'description': description,
#           'u_it_service': it_service_obj.sys_id,
#           'service_offering': service_offering_id,
#           'u_service_provider': organization.sys_id
        }

#       if assignment_group:
#           assignment_group_id = self.get_group(
#               by='name', value=assignment_group).sys_id

#           ticket_data['assignment_group'] = assignment_group_id

#       if caller_name:
#           caller = self.get_user(by='user_name', value=caller_name)
#           ticket_data['caller_id'] = caller.sys_id

        result = self.query_table('sc_task', data=ticket_data,
                                  method='POST')
        return(RequestedItem(self, **result))

    def create_incident_ticket(self, *args, **kwargs):
        """
        Create an incident in ServiceNow

        Args:
        short_description str: Short description for the ticket
        description       str: Long description for the ticket
        state_id          int: State of ticket  (default: 2 (active))
        impact_id         int: Impact of ticket (default: 2 (workgroup))
        urgency_id        int: Urgency of ticket (default: 2 (medium))
        priority_id       int: Priority of ticket (default: 3 (medium))
        assignment_group  str: Assignment group for ticket (default: None)
        caller_name       str: Email of caller (default: 'Joe User')
        service_offering  str: Service Offering
                               (default: Virtual Server Hosting Offering')
        it_service        str: It Service (default: Virtual Server Hosting)
        organization      str: Organization name (default: Duke University)


        Returns:
        Incident object

        """

        short_description = kwargs.get('short_description')
        description = kwargs.get('description')
        state_id = kwargs.get('state_id', 2)
        impact_id = kwargs.get('impact_id', 2)
        urgency_id = kwargs.get('urgency_id', 2)
        priority_id = kwargs.get('priority_id', 3)
        assignment_group = kwargs.get('assignment_group', None)
        caller_name = kwargs.get('caller_name', 'Joe User')
        it_service = kwargs.get('it_service', 'Virtual Server Hosting')
        service_offering = kwargs.get(
            'service_offering', 'Virtual Server Hosting Offering')
        organization_name = kwargs.get('organization', 'Duke University')

        if assignment_group:
            assignment_group_id = self.get_group(
                by='name', value=assignment_group).sys_id

        organization = self.get_organization(by='u_name',
                                             value=organization_name)

        # TODO: Convert services to objects and use that
        it_service_obj = self.get_it_service(by='name', value=it_service,
                                             organization=organization)
        service_offering_id = self.query_table('service_offering?name=%s' %
                                               service_offering)[0]['sys_id']

        ticket_data = {
            'impact': impact_id,
            'urgency': urgency_id,
            'priority': priority_id,
            'contact_type': 'event',
            'incident_state': state_id,
            'short_description': short_description,
            'description': description,
            'u_it_service': it_service_obj.sys_id,
            'service_offering': service_offering_id,
            'u_service_provider': organization.sys_id
        }

        if assignment_group:
            assignment_group_id = self.get_group(
                by='name', value=assignment_group).sys_id

            ticket_data['assignment_group'] = assignment_group_id

        if caller_name:
            print("Trying %s" % caller_name)
            caller = self.get_user(by='name', value=caller_name)
            ticket_data['caller_id'] = caller.sys_id

        result = self.query_table('incident', data=ticket_data, method='POST')
        return(Incident(self, **result))

    @classmethod
    def query_table(self, table, data=None, method='GET', params=None,
                    use_cache=True):
        """
        Do the actual API query here
        """

        url = "https://%s/api/now/table/%s" % (self.base_url, table)
        logging.debug("URL: %s PARAMS: %s METHOD: %s", url, params, method)

        headers = {'Content-Type': 'application/json',
                   'Accept': 'application/json'}

        if use_cache:
            result = requests.request(method, url, auth=self.credentials,
                                      json=data, headers=headers,
                                      params=params)
        else:
            with requests_cache.disabled():
                result = requests.request(method, url, auth=self.credentials,
                                          json=data, headers=headers,
                                          params=params)

        if result.status_code not in [200, 201]:
            print('URL: %s' % url)
            print('Status:', result.status_code, 'Headers:', result.headers,
                  'Error Response:', result.json())
            raise Exception("RequestError")

        try:
            real_result = result.json()['result']
        except Exception as e:
            print("Exception caught: %s\n" % e)
            real_result = result

        return real_result
