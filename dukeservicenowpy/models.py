import json
import operator
import sys


class User(object):
    """
    Object representing a 'User' in Duke ServiceNow
    """

    def __init__(self, api, *args, **kwargs):
        self.raw_data = kwargs
        self.api = api

    def get_task_statistics(self, *args, **kwargs):
        """
        :return: Dictionary of task statistics
        :rtype: dict
        """
        tasks = self.get_tasks(active=False)
        group_stats = {}
        for task in tasks:
            sys_id = task.get_assignment_group()
            if not sys_id:
                continue

            group = self.api.get_group_from_sys_id(sys_id)
            if group.name not in group_stats:
                group_stats[group.name] = 0
            group_stats[group.name] += 1
        return {
            'group_stats': group_stats
        }

    def get_most_active_group(self, *args, **kwargs):
        """
        :return: group that this user has been assigned the most tickets in
        :rtype: Group
        """
        stats = self.get_task_statistics()['group_stats']
        sorted_stats = sorted(stats.items(), key=operator.itemgetter(1))
        group_name = sorted_stats[-1][0]

        return self.api.get_group(by='name', value=group_name)

    def get_tasks(self, *args, **kwargs):
        """
        Get tasks for user

        :param active: Only return active tasks, default True
        :type active: bool
        :return: List of tasks
        :rtype: list
        """

        active = kwargs.get('active', True)

        # Convert boolean to a string for REST

        if active:
            active_str = 'true'
        else:
            active_str = 'false'

        user_sys_id = self.sys_id
        sysparm_query = self.api.build_sysparm_query([
            'assigned_to=%s' % user_sys_id,
            'active=%s' % active_str,
            'ref_incident.incident_stateNOT IN6,7',
            'ORref_incident.incident_stateISEMPTY',
            'sys_class_name!=sc_request',
            'sys_class_name!=sc_req_item',
            'sys_class_name!=u_survey_response_reporting',
            'sys_class_name!=chat_queue_entry'
        ])
        tasks = []
        for task in self.api.query_table(
            'task',
            params={'sysparm_query': sysparm_query}
        ):
            tasks.append(Task(api=self, **task))
        return tasks

    def get_groups(self, *args, **kwargs):
        """
        Get a list of the users groups

        :param attribute: Return a specific attribute, instead of the object
        :type attribute: str
        :param group_max_users: Only return group with total members under
                                this number
        :type group_max_users: int
        :return: List of groups
        :rtype: list
        """
        attribute = kwargs.get('attribute', None)
        group_max_users = kwargs.get('group_max_users', 100)
        groups = []
        results = self.api.query_table(
            'sys_user_grmember',
            params={
                'user': self.raw_data['sys_id']
            }
        )
        for item in results:
            group = self.api.query_table(
                'sys_user_group',
                params={
                    'sysparm_query': "sys_id=%s" % item['group']['value']
                }
            )[0]

            # Don't touch hidden groups
            if group['u_hidden'] == 'true':
                continue

            group_obj = Group(api=self.api, **group)
            if len(group_obj.get_users()) > group_max_users:
                continue

            # Skip groups that aren't assignable
            if group_obj.group_type and (
                    group_obj.group_type['name'] != 'assignment'):
                continue

            if attribute:
                groups.append(getattr(group_obj, attribute))
            else:
                groups.append(group_obj)
        return groups

    def __getattr__(self, key):
        return self.raw_data[key]

    def __repr__(self):
        return u"<DukeServiceNow.User.%s object>" % self.raw_data['name']

    def json(self):
        return(json.dumps(self.raw_data))


class Group(object):
    """
    Object represneting a Group in Duke ServiceNow
    """

    def __init__(self, api, *args, **kwargs):
        """
        """
        blacklist_groups = ['Scrum Team Member - SDLC Product']
        self.api = api
        self.raw_data = kwargs

        possible_group_type = self.api.query_table(
            'sys_user_group_type', params={
                'sysparm_query': 'sys_id=%s' % self.raw_data['type']
            }
        )
        if len(possible_group_type) == 1:
            self.group_type = possible_group_type[0]
        elif len(possible_group_type) == 0:
            self.group_type = None

        if self.raw_data['name'] in blacklist_groups:
            # We are just gonna exit here, no exceptions to be caught
            sys.stderr.write(
                (
                    "You have chanced upon a forbidden S/N group.  Please "
                    "ensure that you never pull a group with this name again"
                    " or you will be cursed with a million S/N tickets and "
                    "calls from angry users\n"
                    "Group: %s\n" % self.raw_data['name']
                )
            )
            sys.exit(66)
        self.api = api

    def get_users(self, *args, **kwargs):
        """
        Get a list of the users groups

        :param attribute: Return a specific attribute, instead of the object
        :type attribute: str
        :return: List of users
        :rtype: list
        """
        attribute = kwargs.get('attribute', None)
        users = []
        results = self.api.query_table(
            'sys_user_grmember',
            params={
                'group': self.raw_data['sys_id']
            }
        )
        for item in results:
            users = self.api.query_table(
                'sys_user',
                params={
                    'sysparm_query': "sys_id=%s" % item['user']['value']
                }
            )
            if len(users) != 1:
                continue

            user = users[0]

            user_obj = User(api=self, **user)
            if attribute:
                users.append(getattr(user_obj, attribute))
            else:
                users.append(user_obj)
        return users

    def __getattr__(self, key):
        return self.raw_data[key]

    def __repr__(self):
        return u"<DukeServiceNow.Group.%s object>" % self.raw_data['name']

    def json(self):
        return(json.dumps(self.raw_data))


class Task(object):
    """
    Object represneting a Task in Duke ServiceNow
    """

    def __init__(self, api, *args, **kwargs):
        self.raw_data = kwargs
        self.api = api

    def get_assignment_group(self):
        """
        :return: Return the assignement group Object, or None
        :rtype: Group
        """
        group_raw = self.raw_data['assignment_group']

        # Empty string means no group...I think
        if isinstance(group_raw, str):
            return None

        group_sys_id = group_raw['value']

        return group_sys_id

    def __getattr__(self, key):
        return self.raw_data[key]

    def __repr__(self):
        short_description = self.raw_data['short_description']
        return u"<DukeServiceNow.Task.%s object>" % short_description

    def json(self):
        return(json.dumps(self.raw_data))


class RequestedItem(object):
    """
    Object represneting a RequestedItem in Duke ServiceNow
    """

    def __init__(self, api, *args, **kwargs):
        self.raw_data = kwargs
        self.api = api

    def get_assignment_group(self):
        """
        :return: Return the assignement group Object, or None
        :rtype: Group
        """
        group_raw = self.raw_data['assignment_group']

        # Empty string means no group...I think
        if isinstance(group_raw, str):
            return None

        group_sys_id = group_raw['value']

        return group_sys_id

    def __getattr__(self, key):
        return self.raw_data[key]

    def __repr__(self):
        number = self.raw_data['number']
        return u"<DukeServiceNow.RequestedItem.%s object>" % number

    def json(self):
        return(json.dumps(self.raw_data))


class Incident(object):
    """
    Object represneting an Incident in Duke ServiceNow
    """

    def __init__(self, api, *args, **kwargs):
        self.raw_data = kwargs
        self.api = api

    def get_assignment_group(self):
        """
        :return: Return the assignement group Object, or None
        :rtype: Group
        """
        group_raw = self.raw_data['assignment_group']

        # Empty string means no group...I think
        if isinstance(group_raw, str):
            return None

        group_sys_id = group_raw['value']

        return group_sys_id

    def __getattr__(self, key):
        return self.raw_data[key]

    def __repr__(self):
        number = self.raw_data['number']
        return u"<DukeServiceNow.Incident.%s object>" % number

    def json(self):
        return(json.dumps(self.raw_data))


class Organization(object):
    """
    Object represneting an Organization in Duke ServiceNow
    """

    def __init__(self, api, *args, **kwargs):
        self.raw_data = kwargs
        self.api = api

    def __getattr__(self, key):
        return self.raw_data[key]

    def __repr__(self):
        name = self.raw_data['u_name']
        return u"<DukeServiceNow.Organization.%s object>" % name

    def json(self):
        return(json.dumps(self.raw_data))


class ItService(object):
    """
    Object represneting an ItService in Duke ServiceNow
    """

    def __init__(self, api, *args, **kwargs):
        self.raw_data = kwargs
        self.api = api

    def __getattr__(self, key):
        return self.raw_data[key]

    def __repr__(self):
        name = self.raw_data['name']
        return u"<DukeServiceNow.ItService.%s object>" % name

    def json(self):
        return(json.dumps(self.raw_data))
