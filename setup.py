import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


with open('requirements.txt') as requirements_file:
    install_requirements = requirements_file.read().splitlines()

setup(
    name="python-duke-servicenow",
    version="0.0.21",
    author="Drew Stinnett",
    author_email="drew.stinnett@duke.edu",
    description=("Python API wrapper for Dukes instance of ServiceNow"),
    license="MIT",
    keywords="servicenow",
    packages=find_packages(),
    install_requires=install_requirements,
    long_description=read('README.md'),
)
