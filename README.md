# python-duke-servicenow

The name 'python-servicenow' is taken by an existing project, that's why 'duke'
is shoved in the middle here


## Setup

Make sure you have ~/.servicenow.yaml set up like this:

```yaml
---
duke:
  username: $ lpass show servicenow-api --username
  password: $ lpass show servicenow-api --password
  instance: duke
```

## Usage

Create the API object like this:

```
    from devilparser import rcfile
    from dukeservicenowpy.servicenow import api

    sn_instance = 'duke'
    configinfo = rcfile.parse('~/.servicenow.yaml', sn_instance).contents()
    cred = {}
    cred['username'] = configinfo['username']
    cred['password'] = configinfo['password']
    SN = api('%s.service-now.com' % 'duke', cred)
```

Once created, you can start using it:

```
    user = SN.get_user_from_name('drews')
```

See example scripts in 'example' directory for more uses
